#include <ctime> // Computer Time (number of seconds since 00:00 hours, Jan 1, 1970 UTC)
#include <cmath>
#include <string>
#include <algorithm> // sort function
#include <vector>
#include <fstream>
#include <iostream>

struct entry {
	int randomInteger;
	std::string randomString;
};

// Sort Container by randomInteger function
bool sortByRI( const entry& lhs, const entry& rhs) { return lhs.randomInteger > rhs.randomInteger; }

int main()
{
	// ----------------------------------------
	// ----- Write entries to binary file -----
	std::srand( time(0) ); // seed for rand() function, that makes sure the random numbers won't be the same everytime.
	const int numberOfEntries = 2 * std::pow( 10, 6 );
	const int charactersPerString = 10;
	std::vector<entry> entries(numberOfEntries);
	std::ofstream outbin( "binary.bin", std::ios::binary | std::ios::out | std::ios::trunc );
	for ( int i = 0; i < numberOfEntries; i++ )
	{
		entries[i].randomInteger = rand();
		for ( int j = 0; j < charactersPerString; j++ )
		{
			// Random Integer 33 < randomInteger < 126 (printable ASCII characters)
			// char() converts the integer to the corresponding ASCII Character
			entries[i].randomString += char( rand( ) % ( 126 - 33 ) + 33 );
		}
		outbin.write( reinterpret_cast<const char*> ( &entries[i].randomInteger ), sizeof( entries[i].randomInteger ) );	// http://www.cplusplus.com/reference/ostream/ostream/write/
		outbin.write( entries[i].randomString.c_str(), (std::streamsize)entries[i].randomString.size() + 1 ); // https://www.c-plusplus.net/forum/243259-full
	}
	outbin.close();	// close binary.bin
	entries.clear(); // clear entries
	
	
	// ----------------------------------------
	// ----- Read entries from binary file ----
	std::ifstream inbin( "binary.bin", std::ifstream::binary );
	std::vector<char> b_char( charactersPerString + 1 );
	std::vector<entry> readEntries( numberOfEntries ); // entries read from binary file
	int i = 0;
	int j = 0;
	while ( inbin.read( reinterpret_cast<char*>( &readEntries[i].randomInteger ), 4 )
			&& inbin.read( reinterpret_cast<char*> ( &b_char[j] ), charactersPerString )
			&& i < numberOfEntries-1 )
	{
		inbin.ignore( );								// skip the padding byte
		for ( j = 0; j < charactersPerString; j++ )
		{
			readEntries[i].randomString += b_char[j];
		}
		i++;
		j = 0;
	}
	
	
	// ----------------------------------------
	// ----- Sort entries in decreasing order -
	std::sort( readEntries.begin( ), readEntries.end( ), sortByRI );
	
	
	// ----------------------------------------
	// ----- Write entries to ascii file ------
	std::ofstream outascii("ascii.txt");	
	for ( i = 0; i < readEntries.size( ); i++ )
	{
		outascii << readEntries[i].randomInteger << "\t" << readEntries[i].randomString << "\r\n";
	}
	
	
	return 0;
}