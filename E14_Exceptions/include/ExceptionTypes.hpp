#ifndef _EXCEPTIONTYPES_HPP_
#define _EXCEPTIONTYPES_HPP_

#include <vector>

struct SmallDivisor // class for small divisor exceptions
{
public:
	static const double Small = 1.0e-12; // smallest divisor
	double sd;
	std::vector<double> myResults;
	SmallDivisor(  std::vector<double> results, double d = 0.0 ) //constructor
	{
		sd = d;
		myResults = results;
	}
};

// const double SmallDivisor::Small = 1.0e-12;

struct ZeroDivisor //class for zero divisor exceptions
{
public:										
	double zero;
	ZeroDivisor( double d = 0 ) { zero = d; } // constructor
};

#endif