#ifndef _QUADRATICEQUATION_HPP_
#define _QUADRATICEQUATION_HPP_

#include <vector>

class QuadraticEquation // f(x) = ax^2 + bx + c = 0
{
private:
	double myA;
	double myB;
	double myC;
	std::vector<double> solve();

public:
	QuadraticEquation( double a, double b, double c );
	~QuadraticEquation( );
	static void check_solve( QuadraticEquation& eq );
};



#endif