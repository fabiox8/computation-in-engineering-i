#include "QuadraticEquation.hpp"
#include "ExceptionTypes.hpp"
#include <cmath>
#include <iostream>

using namespace std;

QuadraticEquation::QuadraticEquation( double a, double b, double c ) :
    myA(a),
	myB(b),
	myC(c)
{
}

QuadraticEquation::~QuadraticEquation()
{
}

std::vector<double> QuadraticEquation::solve()
{
	// SOLVE

	// evaluate the roots of the equation ax^2 + bx + c = 0
	// considering the three cases
	// 1. a < 0
	// 2. a = 0
	// 3. a > 0

    // Create a vector for the two results
	std::vector<double> results( 2 );
	double p = myB / myA;
	double q = myC / myA;
	
    // If the divisor a is small, throw SmallDivisor exception, 
    // passing results and divisor to its constructor
	if ( myA <= SmallDivisor::Small && myA > 0.0 )
	{
        results[0] = - p / 2 + std::sqrt( std::pow( p / 2, 2) - q );
        results[1] = - p / 2 - std::sqrt( std::pow( p / 2, 2) - q );
        throw SmallDivisor( results, myA );
	}
	else if( myA == 0.0 ) // If divisor is zero, throw ZeroDivisor
	{
		throw ZeroDivisor( myA );
		// results[0] = -myC / myB;
		// results[1] = results[0];
	}    
	else // Everything is okay
	{
        results[0] = - p / 2 + std::sqrt( std::pow( p / 2, 2) - q );
        results[1] = - p / 2 - std::sqrt( std::pow( p / 2, 2) - q );
	}

	return results;
}

void QuadraticEquation::check_solve( QuadraticEquation& eq )
{
	
	// First a try-block has to be created
	// Then two exception handler are needed to handle the two exceptions types
	
	try
	{
		std::vector<double> results = eq.solve( );
		std::cout << "First solution is :" << results[0] << std::endl;
		std::cout << "Second solution is :" << results[1] << std::endl;
		return; // normal return if no exceptions thrown
	}
	catch ( SmallDivisor div ) // catch exception of type SmallDivisor
	{
		std::cout << "Small factor in f(x), divisor = " << div.sd << std::endl;
		std::cout << "First solution is :" << div.myResults[0] << std::endl;
		std::cout << "Second solution is :" << div.myResults[1] << std::endl;
		return;
		throw;
	}
	catch ( ZeroDivisor div ) // catch exception of type ZeroDivisor
	{
		std::cout << "Zero factor in f(x), divisor = " << div.zero << std::endl;
		// std::cout << "The solution is: " << div.myResults[0] << std::endl;		
		return;
		throw;
	}
 	catch (...) // catch exception of any other type
	{
		cout <<"Exception found !"<< "\n" << endl;
		return;
		throw;
	} 
}