#include <iostream>
#include <vector>
#include"QuadraticEquation.hpp"

using namespace std;

int main(int argc, char *argv[])
{
	double a = 1.0;
	double b = 3.0;
	double c = 2.0;

    std::cout << "Solving first equation:" << std::endl;
	QuadraticEquation eq1( a, b, c );
	QuadraticEquation::check_solve( eq1 );

	a = 0;
	b = 3.0;
	c = 4.0;

    std::cout << "Solving second equation:" << std::endl;
	QuadraticEquation eq2( a, b, c );
	QuadraticEquation::check_solve( eq2 );

	a = 1.0e-14;
	b = 2.0;
	c = 1.0;

    std::cout << "Solving third equation:" << std::endl;
    QuadraticEquation eq3( a, b, c );
	QuadraticEquation::check_solve( eq3 );

	return 0;
}

