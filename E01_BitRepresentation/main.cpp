/*
Write a program which reads in an integer number,
converts it to its bit representation and outputs the reuslt

Example:		input:	15
				output:	1111
*/

#include <iostream>
#include <limits>

int main()
{

	int integerNumber;
	const int bits = 32;
	int bitRepresentation[bits];
	
	std::cout << "please input integer number n within the range [" 
		<< "0" << "; " 
		<< std::numeric_limits<int>::max() << "] : ";
	std::cin >> integerNumber;
	
	std::cout << "bit representation of integer number " <<  integerNumber  << " with " << bits << " bits" << std::endl;
	
	for (int i = 0; i < bits; i++)
	{
		// x mod 2 is always 0 or 1
		bitRepresentation[(bits-1) - i] = integerNumber % 2;
		integerNumber = integerNumber / 2;
	}	
	
	for (int i = 0; i < bits; i++)
	{
		std::cout << bitRepresentation[i];
		if ( (i+1)%4 == 0 && (i+1) != bits ) std::cout << " ";
	}
	std::cout << std::endl;
	
	return 0;
}
