#pragma once // https://stackoverflow.com/questions/707920/gcc-compiler-error-redefinition-previously-defined

class Point2D
{
private:
    double x;
    double y;

public:
    Point2D();                                    // default constructor
    Point2D(double r, double s);                  // constructor

    void move(double r, double s);                // member function    
    void move(Point2D point);                     // member function
    void draw() const;                            // member function
    double get_x();
    double get_y();

    friend double norm(Point2D point);            // friend (not nice)
};