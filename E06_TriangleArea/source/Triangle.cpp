#include "Triangle.hpp"
#include <iostream>
#include <cmath> // std::sqrt, std::fabs are C++11 and not C++98, so we need to include this header if we're not explicitly telling gcc to compile with C++11 features

Triangle::Triangle(Point2D v0, Point2D v1, Point2D v2)
{
    vertices = new Point2D[3];    // 3 vertices in a triangle

    vertices[0] = v0;
    vertices[1] = v1;
    vertices[2] = v2;
}

Triangle::~Triangle()
{
    delete[] vertices;
}

// output the triangle
void Triangle::draw() const
{
    vertices[0].draw();
    vertices[1].draw();
    vertices[2].draw();
}

double Triangle::area()
{
    double xa, xb, xc;
    double ya, yb, yc;
    double a = 0.0;

    xa = vertices[0].get_x();
    ya = vertices[0].get_y();
    xb = vertices[1].get_x();
    yb = vertices[1].get_y();
    xc = vertices[2].get_x();
    yc = vertices[2].get_y();

    a = 0.5 * std::fabs((xb - xa)*(yc - ya) - (yb - ya)*(xc - xa));
    return a;
}
