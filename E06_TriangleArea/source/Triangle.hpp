#pragma once // https://stackoverflow.com/questions/707920/gcc-compiler-error-redefinition-previously-defined

#include "Point2D.hpp"

class Triangle
{
    Point2D* vertices;                             // Stores a dynamic array of Point2Ds

public:
    Triangle(Point2D v0, Point2D v1, Point2D v2);  // constructor
    ~Triangle();                                   // destructor 

    void draw() const;                             // member function
    double area();                                 // computes and returns area of triangle
};
