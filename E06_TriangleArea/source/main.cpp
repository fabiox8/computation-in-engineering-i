#include "Triangle.hpp"
#include "Point2D.hpp"

#include <iostream>

int main()
{
    Point2D A(0, 0);        // create 2d point
    Point2D B(1, 0);        //      - " -
    Point2D C(0, 1);        //      - " -

    Triangle D(A, B, C);    // create triangle
    D.draw();            // draw triangle
    std::cout << "area of the triangle: " << D.area() << std::endl;

    return 0;
}

