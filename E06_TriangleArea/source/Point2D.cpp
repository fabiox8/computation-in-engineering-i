#include "Point2D.hpp"
#include <iostream>
#include <cmath> // std::sqrt, std::fabs are C++11 and not C++98, so we need to include this header if we're not explicitly telling gcc to compile with C++11 features

Point2D::Point2D()
{
    x = 0.0;
    y = 0.0;
}

Point2D::Point2D(double r, double s)
{
    x = r;
    y = s;
}

void Point2D::move(double r, double s)
{
    x = r;
    y = s;
}
void Point2D::move(Point2D point)
{
    x = point.x;
    y = point.y;
}

void Point2D::draw() const
{
    std::cout << " (" << x << ", " << y << ") " << std::endl;
}

double norm(Point2D point)
{
    return std::sqrt(point.x*point.x + point.y*point.y);
}

double Point2D::get_x()
{
    return x;
}

double Point2D::get_y()
{
    return y;
}
