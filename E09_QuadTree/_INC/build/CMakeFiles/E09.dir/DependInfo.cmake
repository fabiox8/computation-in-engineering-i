# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/fabio/shortcuts/Desktop/Exercises/E09_QuadTree_INC/source/main.cpp" "/home/fabio/shortcuts/Desktop/Exercises/E09_QuadTree_INC/build/CMakeFiles/E09.dir/source/main.cpp.o"
  "/home/fabio/shortcuts/Desktop/Exercises/E09_QuadTree_INC/source/quadtree.cpp" "/home/fabio/shortcuts/Desktop/Exercises/E09_QuadTree_INC/build/CMakeFiles/E09.dir/source/quadtree.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  "../source"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
