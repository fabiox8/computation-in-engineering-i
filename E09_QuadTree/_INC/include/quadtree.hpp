#include <fstream>
#include <string>

class node
{
private:
	double myXmin, myYmin, myXmax, myYmax;
	int myLevel;
	int* myObjectNumber;
	node* myParent;
	node** myChildren;
	// the convention is
	// myChildren[0] = NW  myChildren[1] = NE
	// myChildren[2] = SW  myChildren[3] = SE
	static unsigned int myNoOfNodes; // static members do not belong to an individual
									 // object, but are shared by all objects of the
									 // class.
	std::string myStringCode;
	
public:
	node( double xmin, double ymin, double xmax, double ymax, 
			node* parent, int level, std::string stringCode = "0" );
	~node( );
			
	void generateQuadtree( int maxLevel );
	void writeTreeToConsole( );
	void writeNodeToConsole( );
	void writeBitstreamToConsole( );
	int* getObjectNumber( );
	node* getObject( int* objectNumber, int numberLength );
	void divideMe( ); // refine( );
	bool amIcut( int noPoints ); // shouldIBeRefined( );
	
	void writeTreeToVtk( const char* filename );
	void writeNodeCodesToVtk( std::ofstream& outfile );
	void writePointsToVtk( std::ofstream& outfile );
};