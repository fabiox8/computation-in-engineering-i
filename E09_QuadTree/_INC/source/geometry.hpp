/*
	explicit geometry of a circle at origin
*/

#include <cmath>

bool g( double x, double y )
{
	double radius = 1.0;
	double centerx = 0.0;
	double centery = 0.0;
	
	//if inside: return true
	return 
	(
		std::sqrt( (x-centerx) * (x-centerx) + (y-centery) * (y-centery) ) <= radius
	);
}