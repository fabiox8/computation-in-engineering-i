/*
	Define the class "node" that can construct a quadtree for an implicit geometry
	
	1. Write a node constructor that takes the min- and max-coordinates of the 
		node, a pointer to its parent and its refinement level
	2. Write a method that generates a quadtree until some refinement level
	3. Write a method that checks, whether the the node should refined or not
	4. Write a method that divides a node into 4 nodes (refine a node)
	5. Write a method to write the tree to the console
	6. Write a method to visualize the tree in Paraview
	
	7. Write a method that returns the number of a certain quadtree
		- Extend the constructor, such that it takes the number of objects and assigns
		  it to myObjectNumber of dynamic array (int*)
		- Implement the getObjectNumber method, which returns myObjectNumber
		- Implement the getObject method, such that it takes the object number as a
		  dynamic array (int*), the length of the array (int) and returns a pointer to
		  the corresponding object (node*)
	
	8. Write a method that returns the bitstream representation of the tree in
	   depth-first scheme
		- Implement the writeBitstreamToConsole method, which runs through the tree
		  in depth-first scheme and writes 1 if the node is a leaf node and 0 otherwise
	
	9. Find a way to improve the efficiency of the amIcut() function
		- The amIcut function is implemented such that it runs through all sample
		  points. Try to improve it.
*/

#include "quadtree.hpp"

int main()
{
	node* myQuadtree = new node( 0.0, 0.0, 1.0, 1.0, NULL, 0 );
    myQuadtree->generateQuadtree( 6 );
    myQuadtree->writeTreeToConsole( );
	myQuadtree->writeTreeToVtk( "myQuadtree.vtk" );
	delete myQuadtree;
	
	return 0;
}