#include "quadtree.hpp"
#include "geometry.hpp"

#include <iostream>

node::node( double xmin, double ymin, double xmax, double ymax, 
			node* parent, int level, std::string stringCode ):
	myXmin( xmin ),
	myYmin( ymin ),
	myXmax( xmax ),
	myYmax( ymax ),
	myParent( parent ),
	myLevel( level ),
	myStringCode( stringCode )
{
	myChildren = new node*[4];
	myNoOfNodes++;
}

node::~node( )
{
	for ( int i = 0; i < 4; i++ )
	{
		delete myChildren[i];
	}
	delete myChildren;
}

unsigned int node::myNoOfNodes = 0;

void node::generateQuadtree( int maxLevel )
{
	if ( myLevel < maxLevel && this->amIcut( 4 ) == true )
	{
		this->divideMe( );
		for ( int i = 0; i < 4; i++ )
		{
			myChildren[i]->generateQuadtree( maxLevel );
		}
	}
}

void node::writeTreeToConsole( )
{
	this->writeNodeToConsole( );
	for ( int i = 0; i < 4; i++ )
	{
		if ( myChildren[i] != NULL )
		{
			myChildren[i]->writeTreeToConsole( );
		}
	}
}

void node::writeNodeToConsole( )
{
	std::cout << "myLevel xmin ymin xmax ymax " << myLevel << "\t" << myXmin << "\t"
	<< myYmin << "\t" << myXmax << "\t" << myYmax << std::endl;
}

void node::writeBitstreamToConsole( )
{
	std::cout << "Bitstream" << std::endl;
}

int* node::getObjectNumber( )
{
	return myObjectNumber;
}

// node* getObject( int* objectNumber, int numberLength )
// {
	
// }

void node::divideMe( )
{
	double xhalf = 0.5 * ( myXmax - myXmin );
	double yhalf = 0.5 * ( myYmax - myYmin );
	int newLevel = this->myLevel+1;
	
	std::string stringCode = myStringCode;
	//NW
	stringCode.append("0");
	myChildren[0] = new node(myXmin,myYmin+yhalf,myXmax-xhalf,myYmax,this,newLevel,stringCode);
	//NE
	stringCode = myStringCode; stringCode.append("1");
	myChildren[1] = new node(myXmin+xhalf,myYmin+yhalf,myXmax,myYmax,this,newLevel,stringCode);
	//SW
	stringCode = myStringCode; stringCode.append("2");
	myChildren[2] = new node(myXmin,myYmin,myXmax-xhalf,myYmax-yhalf,this,newLevel,stringCode);
	//SE
	stringCode = myStringCode; stringCode.append("3");
	myChildren[3] = new node(myXmin+xhalf,myYmin,myXmax,myYmax-yhalf,this,newLevel,stringCode);
}

bool node::amIcut( int noPoints )
{
	// cut if neither all points of the node are inside nor all points are outside
	int insideCounter = 0; // count all points of the node, that are inside
	double dx = ( myXmax - myXmin ) / ( noPoints - 1 );
	double dy = ( myYmax - myYmin ) / ( noPoints - 1);
	
	// check all points that belong to the node
	double currentY = myYmin;
	for ( int i = 0; i < noPoints; i++ )
	{
		double currentX = myXmin;
		for ( int j = 0; j < noPoints; j++ )
		{
			if ( g( currentX, currentY ) == true )
			{
				insideCounter++;
			}
			currentX += dx;
		}
		currentY += dy;
	}
	
	if ( insideCounter != noPoints * noPoints && !( insideCounter == 0 ) )
	{
		return true;
	}
	else
	{
		return false;
	}
}

void node::writeTreeToVtk( const char* filename )
{
	std::ofstream outfile;

	outfile.open( filename );
	outfile << "# vtk DataFile Version 4.2" << std::endl;
	outfile << "Test Data" << std::endl;
	outfile << "ASCII" << std::endl;
	outfile << "DATASET UNSTRUCTURED_GRID" << std::endl;

	//output the points
	outfile << "POINTS " << 4 * myNoOfNodes << " " << "double " << std::endl;
	writePointsToVtk( outfile );
	//output the cells
	outfile << "CELLS " << myNoOfNodes << " " << 5 * myNoOfNodes << std::endl;
	//Quickie: I rely on the order of the points here
	for ( size_t i = 0; i < myNoOfNodes * 4; i += 4 )
	{
		outfile << "4\t"  << i << "\t" << i + 1 << "\t" << i + 2 << "\t"<< i + 3
				<< std::endl;
	}
	//output cell types
	outfile << "CELL_TYPES " << myNoOfNodes << std::endl;
	for ( size_t i = 0; i < myNoOfNodes; i++ )
	{
		outfile << "9" << std::endl;
	}
	outfile << "CELL_DATA " << myNoOfNodes << std::endl;
	outfile << "SCALARS Node_Number double" << std::endl;
    outfile << "LOOKUP_TABLE default" << std::endl;
    writeNodeCodesToVtk( outfile );
	outfile.close();
}

void node::writeNodeCodesToVtk( std::ofstream& outfile )
{
	// visualization fails because of the string conversion :-(
	outfile  << myStringCode << std::endl;
	for ( int i = 0; i < 4; i++ )
	{
		if ( myChildren[i] != NULL)
		{
			myChildren[i]->writeNodeCodesToVtk( outfile );
		}
	}
}

void node::writePointsToVtk( std::ofstream& outfile )
{
	//corner vertices of this cell in a counterclockwise order
	outfile << myXmin << "\t" << myYmin << "\t" << "0" << std::endl;
	outfile << myXmax << "\t" << myYmin << "\t" << "0" << std::endl;
	outfile << myXmax << "\t" << myYmax << "\t" << "0" << std::endl;
	outfile << myXmin << "\t" << myYmax << "\t" << "0" << std::endl;
	for ( int i = 0; i < 4; i++ )
	{
		if ( myChildren[i] != NULL )
		{
			myChildren[i]->writePointsToVtk( outfile );
		}
	}
}