#pragma once

class Point2D
{
private:
    double myX;
    double myY;

public:
    Point2D( double x, double y ) : 
		myX(x), 
		myY(y) 
	{
	}

    void setPoint(double x, double y)
    {
        myX = x;
		myY = y;
    }

    double getX() { return myX; }
    double getY() { return myY; }
};
