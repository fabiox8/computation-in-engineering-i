#pragma once

class Observer; // https://stackoverflow.com/questions/15827921/include-in-header-file-vs-forward-declare-and-include-in-cpp
// #include "observer.hpp"
#include "point2D.hpp"


// ----------------------------------
// ----- Abstract Subject class -----
class Subject
{
protected:
	// a subject can also have multiple observers i.e array of pointers 
	// ( or better stl containers in this case )
	Observer* myObserver;
    int numberOfPoints;
    Point2D** myPoints;

public:
	Subject( Point2D* points[], int num );
    void attach( Observer* observer );
    void notify( );
    Point2D* getPoint( int i ) { return myPoints[i]; }
};


// ----------------------------------
// ----- Concrete class Polygon -----
class Polygon : public Subject
{
public:
	Polygon( Point2D* points[], int num );
    void changePoint( int id, double x, double y );
};