#pragma once

#include "subject.hpp"


// ----------------------------------
// ----- Abstract Observer class ----
class Observer
{
private:
	Subject* object;
	
public:
	Observer( Subject* mod );
    virtual void update() = 0;
    virtual void print() = 0;
    Subject* getObject() { return object; }
};

// ----------------------------------
// ----- Concrete class Visualizer --
class Visualizer : public Observer
{
private:
    int pointNum;
	
public:
    Visualizer( Subject* figure, int numPoints );
    void print();
    void update();
};