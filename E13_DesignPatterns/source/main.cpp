/*

*/

#include "point2D.hpp"
#include "observer.hpp"
#include "subject.hpp"
#include <iostream>

int main( )
{
	// create Points;
    Point2D* vertices[3];
    vertices[0] = new Point2D( 0.0, 0.0 );
    vertices[1] = new Point2D( 4.0, 0.0 );
    vertices[2] = new Point2D( 4.0, 4.0 ); 
	
	//create subject
    Polygon triangle( vertices, 3 );
	
	// create and register observer
    Visualizer outputDevice( &triangle, 3 );

    outputDevice.print( );

    // change a point
    std::cout << "\n change a point \n " << std::endl;

    triangle.changePoint( 2, 6.0, 6.0 );

	return 0;
}