#include "subject.hpp"
#include "point2D.hpp"
#include "observer.hpp" 


// ----------------------------------
// ----- Abstract Subject class -----
Subject::Subject( Point2D* points[], int num ) : 
	myPoints( points ),
	numberOfPoints( num )
{
}

void Subject::attach( Observer* obs ) { myObserver = obs; } // register observer

void Subject::notify() { myObserver->update( ); } // notify observers when change occurs


// ----------------------------------
// ----- Concrete class Polygon -----
Polygon::Polygon( Point2D* points[], int num ) : 
	Subject( points, num )
{
}

void Polygon::changePoint( int id, double x, double y )
{
    myPoints[id]->setPoint( x, y );
    Subject::notify();
}