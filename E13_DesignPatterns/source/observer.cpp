#include "observer.hpp"
#include "subject.hpp"

#include <iostream>

// ----------------------------------
// ----- Abstract Observer class ----
Observer::Observer(Subject* mod) : 
	object( mod )
{
	mod->attach( this ); // register observer
}

// ----------------------------------
// ----- Concrete class Visualizer --
Visualizer::Visualizer( Subject* figure, int numPoints ) :
	Observer( figure ),
	pointNum( numPoints )
{
}

void Visualizer::print( )
{
	for ( int i = 0; i < pointNum; i++ )
	{
		std::cout << "point " << i << " :  ( "
		<< Observer::getObject()->getPoint(i)->getX() << " ,  " 
		<< Observer::getObject()->getPoint(i)->getY() << " ) " 
		<< std::endl;
	}
}

void Visualizer::update()
{
    print();
}