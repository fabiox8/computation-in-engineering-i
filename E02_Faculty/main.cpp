/*
	Compute n! for n>12
	n! = 1*2*3*...*k*...n
	
	Idea: Store digits of temporary result k! in an 1D-array
			(Don't use structures for now)		
*/

#include <iostream>

int main()
{
	int n;
	int digitsOfTemporaryResult[50];
	// Initialization of digits
	digitsOfTemporaryResult[0] = 1; // first digit is always one 1*1=1	
    for (int i = 1; i < 50; i++) // all others are 0 at first
    {
        digitsOfTemporaryResult[i] = 0;
    }
	
	std::cout << "Enter a positive integer:\t";
    std::cin >> n;

	for (int k = 1; k <= n; k++)
	{
		int carry = 0;
		for (int i = 0; i < 50; i++)
		{
			int digitOfOldTemporaryResult = digitsOfTemporaryResult[i];
			digitsOfTemporaryResult[i] = (k * digitOfOldTemporaryResult + carry) % 10; // x mod 10 gives integer [0;9]
			carry = (k * digitOfOldTemporaryResult + carry) / 10;
		}
	}
	
	std::cout << "The Faculty of " << n << " is ";
	
	bool firstDigitIsReached = false; //prevent the output to look like this 000000720
	for (int i = 49; i >= 0; i--)
	{
		if ( digitsOfTemporaryResult[i] != 0 && firstDigitIsReached == false ) 
		{
			std::cout << digitsOfTemporaryResult[i];
			firstDigitIsReached = true;
		}
		else if ( firstDigitIsReached == true )
		{
			std::cout << digitsOfTemporaryResult[i];
		}		
	}
	
	
	return 0;
}


/* Explanation
k=1
1! = 1
digitsOfTemporaryResult = [ 1 0 0 0 ... ]
carry = 0

k=2
2! = 1! * 2 = 2
digitsOfTemporaryResult = [ 2 0 0 0 ... ] (k * digitOfOldTemporaryResult + carry) % 10
carry = 0								  (k * digitOfOldTemporaryResult + carry) / 10

k=3
3! = 2! * 3 = 6
digitsOfTemporaryResult = [ 6 0 0 0 ... ] 1. digit = (3*2 + 0) % 10 = 6
carry = (3*2 + 0) /10 = 0

k=4
4! = 3! * 4 = 24
digitsOfTemporaryResult = [ 4 0 0 0 ... ] 1. digit = (4*6 + 0) % 10 = 4
carry = (4*6 + 0) / 10 = 2				  
digitsOfTemporaryResult = [ 4 2 0 0 ... ] 2. digit = (4*0 + 2) % 10 = 2
carry = 0

k=5
5! = 4! * 5 = 120
digitsOfTemporaryResult = [ 0 2 0 0 ... ] 1. digit = (5*4 + 0) % 10 = 0
carry = (5*4 + 0) / 10 = 2
digitsOfTemporaryResult = [ 0 2 0 0 ... ] 2. digit = (5*2 + 2) % 10 = 2
carry = (5*2 + 2) / 10 = 1
digitsOfTemporaryResult = [ 0 2 1 0 ... ] 3. digit = (5*0 + 1) % 10 = 1
carry = 0

k=6
6! = 5! * 6 = 720
digitsOfTemporaryResult = [ 0 2 1 0 ... ] 1. digit = (6*0 + 0) % 10 = 0
carry = (6*0 + 0) / 10 = 0
digitsOfTemporaryResult = [ 0 2 1 0 ... ] 2. digit = (6*2 + 0) % 10 = 2
carry = (6*2 + 0) / 10 = 1
digitsOfTemporaryResult = [ 0 2 7 0 ... ] 3. digit = (6*1 + 1) % 10 = 7
carry = (6*1 + 1) / 10 = 0

k=7
7! = 6! * 7 = 5040
digitsOfTemporaryResult = [ 0 2 7 0 ... ] 1. digit = (7*0 + 0) % 10 = 0
carry = (7*0 + 0) / 10 = 0
digitsOfTemporaryResult = [ 0 4 7 0 ... ] 2. digit = (7*2 + 0) % 10 = 4
carry = (7*2 + 0) / 10 = 1
digitsOfTemporaryResult = [ 0 4 0 0 ... ] 3. digit = (7*7 + 1) % 10 = 0
carry = (7*7 + 1) / 10 = 5
digitsOfTemporaryResult = [ 0 4 0 5 ... ] 4. digit = (7*0 + 5) % 10 = 5
carry = (7*0 + 5) / 10 = 0

.
.
.

*/