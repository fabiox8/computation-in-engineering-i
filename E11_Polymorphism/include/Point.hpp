// -------------------------------
// ------ Base Class Point -------
class Point
{
public:
	Point( );
	
	virtual double norm( ); // virtual functions can be overridden by derived classes
};


// ---------------------------------
// ------ Derived Class Point2D ----
class Point2D : Point
{
private:
	double x, y;
	
public:
	Point2D( double x, double y );
	
	double norm( ); // this will override the norm function of the base class Point
};


// ---------------------------------
// ------ Derived Class Point3D ----
class Point3D : Point
{
private:
	double x, y, z;

public:
	Point3D( double x, double y, double z );
	
	double norm( ); // this will override the norm function of the base class Point
};