#include "Point.hpp"

#include <cmath>

// -------------------------------
// ------ Base Class Point -------
Point::Point() {}
// It doesn't make sense to actually implement norm() here. In lecture 12 pure virtual
// functions are introduced. That would probably make even more sense here.
double Point::norm() {}

// ---------------------------------
// ------ Derived Class Point2D ----
Point2D::Point2D( double x, double y ) :
	x(x),
	y(y)
{
}
double Point2D::norm( ) { return std::sqrt( x * x + y * y ); } // same name, same arguments, but different implementation


// ---------------------------------
// ------ Derived Class Point3D ----
Point3D::Point3D( double x, double y, double z ) :
	x(x),
	y(y),
	z(z)
{
}
double Point3D::norm( ) { return std::sqrt( x * x + y * y + z * z ); } // same name, same arguments, but different implementation