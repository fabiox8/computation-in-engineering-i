/*
	Implement the classes "Point2D" and "Point3D", that inherit from the base
	class "Point".
	"Point" should have an interface function norm( ) which calculates the distance
	of a given point from the origin.
*/

#include <iostream>

#include "Point.hpp"

int main()
{
	Point2D p1( 1, 2 );
	Point3D p2( 1, 1, 0 );

	std::cout << "norm of p1= " << p1.norm() << std::endl;
	std::cout << "norm of p2= " << p2.norm() << std::endl;
	
	return 0;
}