/*
	main.cpp
	main function of the Pipe Network project
*/
#include <iostream>
#include <fstream>

#include "node.hpp"
#include "tube.hpp"
#include "PipeNet.hpp"
#include "MatVec.hpp"

// main function
int main()
{
	std::cout << "PROGRAM PIPE NETWORK" << std::endl;
	
	
	// -------------------------------------
	// ---------- READ INPUT FILE ----------
	int numberOfNodes, numberOfTubes;
	double x, y, Q;
	int nodeNumberA, nodeNumberB;
	double diameter;

	std::ifstream infile( "pipedata.txt" );
	if (!infile) { std::cout << "error: \"pipedata.txt\" not found.\n"; }

	infile >> numberOfNodes;
	infile >> numberOfTubes;

	Node** vec_nodes = new Node*[numberOfNodes];
	// Node* vec_nodes = new Node[numberOfNodes];
	for ( int i = 0; i < numberOfNodes; i++ )
	{
		infile >> x;
		infile >> y;
		infile >> Q;
		vec_nodes[i] = new Node( i, x, y, Q );
		// vec_nodes[i] = Node( i, x, y, Q );
	}

	Tube** vec_tubes = new Tube*[numberOfTubes];
	// Tube* vec_tubes = new Tube[numberOfTubes];
	for ( int i = 0; i < numberOfTubes; i++ )
	{
		infile >> nodeNumberA;
		infile >> nodeNumberB;
		infile >> diameter;
		vec_tubes[i] = new Tube( i, vec_nodes[nodeNumberA-1], vec_nodes[nodeNumberB-1], diameter ); // nodeNumbers start counting at 1 in pipenetwork.txt, but in C++ we start counting at 0; // default constructor is needed to do this
		// vec_tubes[i] = Tube( i, vec_nodes[nodeNumberA-1], vec_nodes[nodeNumberB-1], diameter );
	}	


// ---------- CREATE PIPENETWORK -------
// -------------------------------------	
	//PipeNet* pipeNetwork = new PipeNet( vec_nodes, vec_tubes, numberOfNodes, numberOfTubes );
	PipeNet pipeNetwork( vec_nodes, vec_tubes, numberOfNodes, numberOfTubes );


// ---------- CALCULATE FLUXES -------
// -------------------------------------		
	//Vcr fluxes = pipeNetwork->calcFlux( );
	Vcr fluxes = pipeNetwork.calcFlux( );


// ---------- OUTPUT RESULTS -----------
// -------------------------------------	
	for ( int i = 0; i < fluxes.size( ); i++ ) { std::cout << fluxes[i] << std::endl; }

	return 0;
}