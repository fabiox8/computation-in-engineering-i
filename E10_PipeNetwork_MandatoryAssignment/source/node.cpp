﻿#include <iostream>

#include "node.hpp"

Node::Node( int num, double x, double y, double Q ):
	num(num),
	x(x),
	y(y),
	Q(Q)
{
	// this->num = num;
	// 	this->x = x;
	// 	this->y = y;
	//	this->Q = Q;
	std::cout << "Node( int num, double x, double y, double Q )" << std::endl;
}

int Node::getNum( ) { return num; }
double Node::getX( ) { return x; }
double Node::getY( ) { return y; }
double Node::getQ( ) { return Q; }