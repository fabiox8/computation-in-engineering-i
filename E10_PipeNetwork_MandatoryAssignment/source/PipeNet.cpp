﻿#include <iostream>

#include "PipeNet.hpp"

PipeNet::PipeNet( Node** vec_nodes, Tube** vec_tubes, int n_nodes, int n_tubes ):
				vec_nodes( vec_nodes ),
				vec_tubes( vec_tubes ),
				n_nodes( n_nodes ),
				n_tubes( n_tubes )
{
// 	this->vec_nodes = vec_nodes;
// 	this->vec_tubes = vec_tubes;
// 	this->n_nodes = vec_nodes.size( );
// 	this->n_tubes = vec_tubes.size( );
std::cout << "PipeNet( Node** vec_nodes, Tube** vec_tubes, int n_nodes, int n_tubes )" << std::endl;
}

PipeNet::~PipeNet( )
{
	for ( int i = 0; i < n_nodes; i++ ) { delete[] vec_nodes[i]; }
	delete[] vec_nodes;
	for ( int i = 0; i < n_tubes; i++ ) { delete[] vec_tubes[i]; }
	delete[] vec_tubes;
	std::cout << "~PipeNet( )" << std::endl;
}

Vcr PipeNet::calcFlux( )
{
	Mtx B( n_nodes, 0 ); // permeability matrix B
	Vcr h( n_nodes, 0 ); // hydraulic head vector h -> unknown! Calculated by solving B h = -Q
	Vcr Q( n_nodes, 0 ); // load vector Q
	Vcr q( n_tubes, 0 ); // flux vector q
	Node* nodeA;
	Node* nodeB; // pointers to nodeA and nodeB of a tube
	int nA, nB; // numbers of nodeA and nodeB of a tube
	double Bi; // permeability of a tube

	
	// Assemble the permeability matrix B
	for ( int i = 0; i < n_tubes; i++ )
	{
		nodeA = vec_tubes[i]->getNodeA( ); // for this the operator= has to be defined
		nodeB = vec_tubes[i]->getNodeB( );
		nA = nodeA->getNum( );
		nB = nodeB->getNum( );
		Bi = vec_tubes[i]->calcB( );
		B[nA][nA] += Bi; 
		B[nB][nB] += Bi;
		B[nA][nB] -= Bi;
		B[nB][nA] -= Bi;
	}
	
	// Create the load vector Q
	// -=, bc B.GaussElim(-Qi) is not possible
	for ( int i = 0; i < n_nodes; i++ ) { Q[i] -= vec_nodes[i]->getQ( ); };
	
	// Apply boundary condition
	// absolute height of first node is 0
	for ( int i = 0; i < n_nodes; i++ )
	{
		B[i][0] = 0.0;
		B[0][i] = 0.0;
	}
	B[0][0] = 1.0;
	//Q[0] = 0.0;
	
	// Solve the linear equation system to get hydraulic head vector h
	B.GaussElim( Q ); // the result is stored in Q
	h = Q;

	// Calculate the flux vector q
	for ( int i = 0; i < n_tubes; i++ )
	{
		nodeA = vec_tubes[i]->getNodeA( );
		nodeB = vec_tubes[i]->getNodeB( );
		nA = nodeA->getNum( );
		nB = nodeB->getNum( );
		Bi = vec_tubes[i]->calcB( );
		q[i] = Bi * ( h[nA] - h[nB] );
	}

	return q;
}