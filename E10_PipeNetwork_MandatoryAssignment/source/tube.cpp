﻿#include "tube.hpp"
#include "node.hpp"

#include <cmath>
#include <iostream>

Tube::Tube( int num, Node* nodeA, Node* nodeB, double d ):
			num( num ),
			nodeA( nodeA ),
			nodeB( nodeB ),
			d( d)
{
// 	this->num = num;
// 	this->nodeA = nodeA;
// 	this->nodeB = nodeB;
// 	this->d = d;
	std::cout << "Tube( int num, Node* nodeA, Node* nodeB, double d )" << std::endl;
}

double Tube::calcLength( )
{
	double xA, xB, yA, yB, x, y;
	xA = nodeA->getX( );
	xB = nodeB->getX( );
	yA = nodeA->getY( );
	yB = nodeB->getY( );
	x = xA - xB;
	y = yA - yB;
	return std::sqrt( x * x + y * y );
}

double Tube::calcB( )
{
	const double pi = std::atan( 1 ) * 4;
	double g = 9.81;
	double v = 10^(-6);
	double l = this->calcLength( );
	return pi * g * std::pow( d, 4 ) / ( 128 * v * l );
}

Node* Tube::getNodeA( ) { return nodeA; }
Node* Tube::getNodeB( ) { return nodeB; }