#pragma once

#include "node.hpp"
#include "tube.hpp"
#include "MatVec.hpp"

class PipeNet
{
private:
    Node** vec_nodes;
    Tube** vec_tubes;
    int n_nodes;
    int n_tubes;

public:
    PipeNet( Node** vec_nodes, Tube** vec_tubes, int  n_nodes, int n_tubes );
	~PipeNet( );

    Vcr calcFlux( );
};