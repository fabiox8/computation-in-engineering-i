#pragma once

#include "node.hpp"

class Tube
{
private:
	int num;
	Node* nodeA;
	Node* nodeB;
	double d;
	double q;

public:
	Tube( int num, Node* nodeA, Node* nodeB, double d );

	double calcLength( );
	double calcB( );
	
	Node* getNodeA( );
	Node* getNodeB( );
};