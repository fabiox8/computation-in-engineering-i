#pragma once

class Node
{
private:
	int num;
	double x, y, Q;

public:
	Node( int num, double x, double y, double Q );	

	int getNum( );
	double getX( );
	double getY( );
	double getQ( );
};