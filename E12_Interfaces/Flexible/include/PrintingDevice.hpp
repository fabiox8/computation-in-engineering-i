#pragma once

#include "point.hpp"

// ------------------------------------
// ---------- PrintingDevice ----------
class PrintingDevice // Abstract base class PrintingDevice
{
public:
	PrintingDevice() {};
	virtual ~PrintingDevice() {};
	virtual void printPoint( Pt2d& pt ) = 0;
	virtual void printPoint( Pt3d& pt ) = 0;
	virtual void printLine( Pt2d& ptA, Pt2d& ptB ) = 0;
};


// ------------------------------------
// ---------- ConsolePrintingDevice ---
class ConsolePrintingDevice : public PrintingDevice
{
public:
	ConsolePrintingDevice() {};
	~ConsolePrintingDevice() {};
	void printPoint( Pt2d& pt );
	void printPoint( Pt3d& pt );
	void printLine( Pt2d& ptA, Pt2d& ptB );
};

// ------------------------------------
// ---------- FilePrintingDevice ------
class FilePrintingDevice : public PrintingDevice
{
public:
	FilePrintingDevice() {};
	~FilePrintingDevice() {};
	void printPoint( Pt2d& pt );
	void printPoint( Pt3d& pt );
	void printLine( Pt2d& ptA, Pt2d& ptB );
};