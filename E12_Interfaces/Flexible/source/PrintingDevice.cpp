#include <fstream>

#include "PrintingDevice.hpp"

// ------------------------------------
// ---------- ConsolePrintingDevice ---
void ConsolePrintingDevice::printPoint( Pt2d& pt )
{
	std::cout << "(" << pt.getX() << "," << pt.getY() << ")" << std::endl;
}

void ConsolePrintingDevice::printPoint( Pt3d& pt )
{
	std::cout << "(" << pt.getX() << "," << pt.getY() << "," << pt.getZ() << ")" << std::endl;
}

void ConsolePrintingDevice::printLine( Pt2d& ptA, Pt2d& ptB ) 
{ } // empty function - must be provided in derived class


// ------------------------------------
// ---------- FilePrintingDevice ------
void FilePrintingDevice::printPoint( Pt2d& pt )
{
	std::ofstream outfile;
	outfile.open( "output.txt" );
	outfile << "(" << pt.getX() << "," << pt.getY() << ")" << "\r\n";
	outfile.close( );
}

void FilePrintingDevice::printPoint( Pt3d& pt ) {};
void FilePrintingDevice::printLine( Pt2d& ptA, Pt2d& ptB ) {};