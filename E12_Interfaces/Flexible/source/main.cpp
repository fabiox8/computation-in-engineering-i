#include "shape.hpp"
#include "PrintingDevice.hpp"

#include <iostream>

int main( )
{
	Shape* s[3];
	s[0] = new Circle( Pt2d( 0, 0 ), 1 );
	s[1] = new Circle( Pt2d( 10, 5 ),3 );
	
	Pt2d pts[3];
	pts[0] = Pt2d( 0, 0 );
	pts[1] = Pt2d( 2, 1 );
	pts[2] = Pt2d( 3, 0 );
	s[2] = new Triangle( pts );
    
    //adressing the common interface
	// create and use printing device
	ConsolePrintingDevice* console = new ConsolePrintingDevice;
	FilePrintingDevice* file = new FilePrintingDevice;
	for ( int i = 0; i < 3; i++ )
	{
		s[i]->draw( console );		
		s[i]->draw( file );
	}
	
	return 0;
}