#include "shape.hpp"

#include <iostream>

// -----------------------------
// ---------- Circle -----------
Circle::Circle( Pt2d p, double d ) :
	center( p ),
	radius( d )
{
}

void Circle::draw( PrintingDevice* pd )
{
	pd->printPoint( center );	
}


// -----------------------------
// ---------- Triangle ---------
Triangle::Triangle( Pt2d* p )
{
	vertices = new Pt2d[3];
	for ( int i = 0; i < 3; i++ ) { vertices[i] = p[i]; }
}

void Triangle::rotate( int i ) {	/*not implemented yet*/ }

void Triangle::draw( PrintingDevice* pd )
{    
	for ( int i = 0; i < 3; i++ ) { pd->printPoint( vertices[i] ); }	
}