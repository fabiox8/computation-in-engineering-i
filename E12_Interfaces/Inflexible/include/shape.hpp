#include "point.hpp"


// -----------------------------
// ---------- Shape ------------
// Abstract base class Shape. No object of this class can be
// constructed, because it contains pure virtual functions (=0)
// which serve as an interface for all derived classes
class Shape
{
public:
	virtual void draw( ) = 0;			//pure virtual function
	virtual void rotate( int i ) = 0;	//rotate i degrees
	virtual bool is_closed( ) = 0;		//pure virtual function
	virtual ~Shape( ) { }; 				//empty virtual destructor
};


// -----------------------------
// ---------- Circle -----------
class Circle : public Shape				//derived class for circles
{
private:
	Pt2d center;						//center of circle
	double radius;						//radius of circle
public:
	Circle( Pt2d p, double d ); 		//constructor
	void rotate( int i ) {};			//override Shape::rotate()
	void draw( );						//override Shape::draw();
	bool is_closed( ) { return true; }	//a circle is closed
	~Circle( ) {}
};


// -----------------------------
// ---------- Polygon ----------
class Polygon: public Shape 					//abstract class
{
public:
	bool is_closed( ) { return true; }	//override Shape::is_closed()
										//but draw&rotate still undefined
};


// -----------------------------
// ---------- Triangle ---------
class Triangle: public Polygon
{
private:
	Pt2d* vertices;
public:
	Triangle( Pt2d* );
	~Triangle( ) { delete vertices; }
	void rotate( int i );				//override Shape::rotate()
	void draw( );						//override Shape::draw()
};


// Inheritance types 
// https://www.programiz.com/cpp-programming/public-protected-private-inheritance