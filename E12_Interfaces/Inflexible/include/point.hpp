#include <iostream>


// -----------------------------
// ---------- Point ------------
class Pt 
{
protected: // protected members are visible to derived classes, while private members are not
	double x;
public:
	Pt( double a );
	virtual void draw( ); 
};


// -----------------------------
// ---------- Point2D ----------
class Pt2d : public Pt 
{
protected:
	double y;
public:
	Pt2d( double a = 0, double b = 0 );
	void draw( );
};


// -----------------------------
// ---------- Point3D ----------
class Pt3d : public Pt2d
{
private:
	double z;
public:
	Pt3d( double a = 0, double b = 0, double c = 0 );
	void draw();
};
