#include "shape.hpp"

#include <iostream>

// -----------------------------
// ---------- Circle -----------
Circle::Circle( Pt2d p, double d ) :
	center( p ),
	radius( d )
{
}

void Circle::draw()
{
	std::cout << "circle: " ;
	center.draw( );
	std::cout<< " r = " << radius;
}


// -----------------------------
// ---------- Triangle ---------
Triangle::Triangle( Pt2d* p )
{
	vertices = new Pt2d[3];
	for ( int i = 0; i < 3; i++ ) { vertices[i] = p[i]; }
}

void Triangle::rotate( int i ) {	/*not implemented yet*/ }

void Triangle::draw()
{
    std::cout << "tri: " ;
	for ( int i = 0; i < 3; i++ ) { vertices[i].draw( ); }
	std::cout << std::endl;
}