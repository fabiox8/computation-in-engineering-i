#include "point.hpp"

#include <iostream>

// -----------------------------
// ---------- Point ------------
Pt::Pt( double a ) :
	x( a )
{
}
void Pt::draw() { std::cout << "(" << x << ")" << std::endl; } // virtual is only used in class declaration

// -----------------------------
// ---------- Point2D ----------
Pt2d::Pt2d( double a, double b ) :
	Pt( a ),
	y( b )
{			
}
void Pt2d::draw( ) { std::cout << "(" << x << "," << y << ")" << std::endl; }

// -----------------------------
// ---------- Point3D ----------
Pt3d::Pt3d( double a, double b, double c ) :
	Pt2d( a, b ),
	z( c )
{			
}
void Pt3d::draw( ) { std::cout << "(" << x << "," << y << "," << z << ")" << std::endl; }