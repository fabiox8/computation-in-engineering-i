cmake_minimum_required(VERSION 3.5)

project(E12)

include_directories(include)
include_directories(source)

add_executable(
	E12
	source/main.cpp
	source/point.cpp
	source/shape.cpp
)