/*
	Implement the functions:
		- trapezoidalRule(...)
		- setIntegrator(...)
		- evaluateIntegral(...)
		
	Compare the relative error of the midpointrule and the trapezoidalrule for
	f(x) = x^2 in the interval [a,b] = [0,1]
*/

#include <iostream>
#include <cmath>

typedef double(*integrandFptr) (double); //integrandFptr is a pointer to a function which gets a double and returns a double
typedef double(*integrationSchemeFptr) (integrandFptr, double, double); //integrationSchemeFptr is a pointer to a function which gets an integrandFptr a double and a double

struct Integrator
{
	integrandFptr myIntegrand; // pointer to the function that should be integrated
	integrationSchemeFptr myIntegrationScheme; // pointer to the function of the integration scheme
	int mySegments;
	
	double evaluateIntegral( double a, double b )
	{
		double x1, x2;
		double stepSize = ( b - a ) / mySegments;
		double result = 0.0;
		
		for (int i = 0; i < mySegments; i++)
		{
			x1 = a + stepSize * i;
			x2 = a + stepSize * (i+1);
			result += myIntegrationScheme(myIntegrand, x1, x2);
		}		
		return result;
	}
	
	void setIntegrator( integrandFptr integrand, integrationSchemeFptr integrationScheme)
	{
		myIntegrand = integrand;
		myIntegrationScheme = integrationScheme;	
	}
};

double evaluateF(double x) // f(x) = x^2
{
	return x*x;
}

double evaluatef2(double x) // f2(x) = x^2 +1
 {
	return x*x + 1;
 }

double midPointRule( integrandFptr integrand, double x1, double x2 )
{
	return ((*integrand)(0.5*(x1 + x2))*(x2 - x1));
}

double trapezoidalRule(integrandFptr integrand, double x1, double x2)
{
	return (0.5*((*integrand)(x1) + (*integrand)(x2))*(x2 - x1));
}

int main()
{	
	int n = 10;
	double a = 0;
	double b = 1;

	double resultMidPointRule;
	double resultTrapezoidalRule;
	double exactResult = 1.0 / 3.0;
	
	Integrator myIntegrator;
	myIntegrator.mySegments = n;
	
	myIntegrator.setIntegrator( &evaluateF, &midPointRule );
	resultMidPointRule = myIntegrator.evaluateIntegral( a, b );
	
	myIntegrator.setIntegrator( &evaluateF, &trapezoidalRule );
	resultTrapezoidalRule = myIntegrator.evaluateIntegral( a, b );
	
	double errorMidPointRule = fabs(resultMidPointRule - exactResult) / exactResult;
	double errorTrapezoidalRule = fabs(resultTrapezoidalRule - exactResult) / exactResult;
	
	std::cout << "The relative error from the midPointRule is: " << errorMidPointRule << std::endl;
	std::cout << "The relative error from the trapezoidalRule is: " << errorTrapezoidalRule << std::endl;
	
	return 0;
}
