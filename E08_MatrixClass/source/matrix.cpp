#include <cmath>
#include <iostream>

#include "matrix.hpp"

matrix::matrix( int m, int n, double** mat )
{
	rows = m;
	cols = n;
	entries = new double*[rows];
	for ( int m = 0; m < rows; m++ )
	{
		entries[m] = new double[cols];
		for ( int n = 0; n < cols; n++ )
		{
			entries[m][n] = mat[m][n];
		}
	}
}

matrix::matrix( const matrix& mat )
{
	rows = mat.rows;
	cols = mat.cols;
	entries = new double*[rows];
	for ( int m = 0; m < rows; m++ )
	{
		entries[m] = new double[cols];
		for ( int n = 0; n < cols; n++ )
		{
			entries[m][n] = mat.entries[m][n];
		}
	}
}

matrix& matrix::operator=( const matrix& mat )
{
	if ( this != &mat )
	{
		rows = mat.rows;
		cols = mat.cols;
		for ( int m = 0; m < rows; m++ )
		{
			for ( int n = 0; n < cols; n++ )
			{
				entries[m][n] = mat.entries[m][n];
			}
		}
	}
	return *this;
}

matrix::~matrix()
{
	for ( int m = 0; m < rows; m++ )
	{
		delete[] entries[m];
	}
	delete[] entries;
}

double matrix::one_norm() // "Spalten Norm"
{
	double one_norm = 0;
	double sum;

	for ( int n = 0; n < cols; n++ ) 
	{
		sum = 0;
		for ( int m = 0; m < rows; m++ )
		{
			sum += std::fabs( entries[m][n] );
		}
		if ( sum > one_norm )
		{
			one_norm = sum;
		}
	}

	return one_norm;
}

double matrix::max_norm() 
{
	double max_norm = 0;
	double sum;
	
	for (int m = 0; m < rows; m++) 
	{
		sum = 0;
		for (int n = 0; n < cols; n++)
		{
			sum += std::fabs( entries[m][n] );
		}
		if ( sum > max_norm )
		{
			max_norm = sum;
		}
	}

	return max_norm;
}

double matrix::fro_norm() {
	double fro_norm = 0;
	for ( int m = 0; m < rows; m++ )
	{
		for ( int n = 0; n < cols; n++ )
		{
			fro_norm += entries[m][n] * entries[m][n];
		}
	}

	return std::sqrt(fro_norm);
}

void matrix::draw()
{	
	for ( int m = 0; m < rows; m++ )
	{
		for ( int n = 0; n < cols; n++ )
		{
			std::cout << entries[m][n] << "\t";
		}
		std::cout << std::endl;
	}
}