class matrix
{
private:
	double** entries;
	int rows;
	int cols;

public:
	matrix( int m, int n, double** mat); //constructor
	matrix( const matrix& mat ); //copy constructor
	matrix& operator=( const matrix& mat ); //assignment operator
	~matrix(); //destructor
	
	double one_norm();
	double max_norm();
	double fro_norm();
	void draw();
};