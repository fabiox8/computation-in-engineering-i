/*
	Define a matrix class that contains a double pointer for the entries, two integers
	for the dimensions (number of rows / columns) of the matrix, and one-, max-,
	and frobenius-norm functions.
*/

#include "matrix.hpp"

#include <iostream>

int main()
{
	int rows;
	int cols;
	std::cout << "Number of rows:" << std::endl;
	std::cin >> rows;
	std::cout << "Number of columns:" << std::endl;
	std::cin >> cols;
	double** arrayOfArrays = new double*[rows];
	for ( int m = 0; m < rows; m++)
	{
		arrayOfArrays[m] = new double[cols];
		for ( int n = 0; n < cols; n++ )
		{
			std::cout << "Row " << m << ", column " << n << ":" <<std::endl;
			std::cin >> arrayOfArrays[m][n];
		}
	}
	
	// -----
		std::cout << "arrayOfArrays: " << arrayOfArrays << std::endl;
		std::cout << "&arrayOfArrays[0]: " << &arrayOfArrays[0] << std::endl << std::endl;

		std::cout << "arrayOfArrays[0]: " << arrayOfArrays[0] << std::endl;
		std::cout << "&arrayOfArrays[0][0]: " << &arrayOfArrays[0][0] << std::endl;
		std::cout << "arrayOfArrays[1]: " << arrayOfArrays[1] << std::endl;
		std::cout << "&arrayOfArrays[1][0]: " << &arrayOfArrays[1][0] << std::endl << std::endl;
	
		std::cout << "arrayOfArrays[0][0]: " << arrayOfArrays[0][0] << std::endl;
		std::cout << "arrayOfArrays[0][1]: " << arrayOfArrays[0][1] << std::endl;
		std::cout << "arrayOfArrays[1][0]: " << arrayOfArrays[1][0] << std::endl;
		std::cout << "arrayOfArrays[1][1]: " << arrayOfArrays[1][1] << std::endl;
	// -----
	
	matrix mat1(rows, cols, arrayOfArrays); //call the constructor	
	delete[] arrayOfArrays; //free the dynamic memory of the entries array
	
	std::cout << "mat1: " << std::endl;
	mat1.draw();
	
	std::cout << "One-norm of mat1: " << mat1.one_norm() << std::endl;
	std::cout << "Max-norm of mat1: " << mat1.max_norm() << std::endl;
	std::cout << "Forbenius-norm of mat1: " << mat1.fro_norm() << std::endl;
	
	std::cout << "mat2: " << std::endl;
	matrix mat2( mat1 );
	mat2.draw();
	
	std::cout << "mat3: " << std::endl;
	matrix mat3 = mat1;
	mat3.draw();
}