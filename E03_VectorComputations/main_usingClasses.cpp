/*
	Write a C++ programm to perform basic vector computations.
	The dimensions of the vectors should be specified by the user during execution of the program.
	Use dynamic arrays.
	Implement input/output, scalar product, euclidean norm and maximum
	
	Define a struct for the vector and a separate function for each task
*/


#include <iostream>
#include <cmath>

class vector			// the main difference between a class and a struct is
{						// that members of a class are private by default
public:					// whereas the members of a struct are public by default.
	double* entries;	// Both can hold data and functions
	int size;
};

vector input()
{
	vector v;
	std::cout << "size = "; 
	std::cin >> v.size;
	v.entries = new double[v.size];
	for (int i = 0; i < v.size; i++)
	{
		std::cout << "entry " << i << " = ";
		std::cin >> v.entries[i];
	}
	return v;
}

void output(vector& v)
{
	for (int i = 0; i < v.size; i++)
	{
		std::cout << v.entries[i] << std::endl;
	}
}

double scalar(vector& v1, vector& v2)
{
	double result = 0.0;
	if (v1.size == v2.size)
	{
		for (int i = 0; i < v1.size; i++)
		{
			result += v1.entries[i] * v2.entries[i];
		}
	}
	else std::cout << "v1 and v2 do not have the same dimensions!" << std::endl;
	return result;
}

double euclid(vector& v)
{
	double result = 0.0;
	for (int i = 0; i < v.size; i++)
	{
		result += v.entries[i] * v.entries[i];
	}
	return std::sqrt(result);
}

double max(vector& v)
{
	double max = v.entries[0];
	for( int i = 1; i < v.size; i++ )
	{
		if ( max < v.entries[i] ) max = v.entries[i];
	}
	return max;
}

int main()
{
	vector myVector = input();
	output(myVector);
	double scalarProduct = scalar(myVector, myVector);
	std::cout << "scalarProduct = "  << scalarProduct << std::endl;
	double euclideanNorm = euclid(myVector);
	std::cout << "euclideanNorm = " << euclideanNorm << std::endl;
	double maxEntry = max(myVector);
	std::cout << "maxEntry = " << maxEntry << std::endl;
}
