/*
C++ library that computes oriented/ axis aligned bounding box
	on an unorganized set of points

compiled to .dll file

Bounding box
	defined by min/ max coordinates in directions of coordinate system
	(world coordinates  (x_w, y_w) -> axis aligned)
	(object coordinates (x_o, y_o) -> oriented)

Steps (axis aligned)
	*1. compute sample points p_i on the curve -> store in matrix P (slide 8)
	*2. find column-wise minimum values -> gives px_min, py_min, pz_min
	*3. find column-wise maximum values -> gives px_max, py_max, pz_max
		=> store in matrix Q [8x3], bc 3D = 8 points � xyz-coords (slide 13)

Steps (oriented)
	*1. compute sampel points p_i on the curve -> store in matrix P (slide 9)
	2. compute column-wise mean valuess -> gives px_mean, py_mean, pz_mean
		-> center of gravity - store in c = [c_x, c_y, c_z]
	3. set up symmetric covariance matrix C (slide 10)
	4. compute eigenvectors of covariance matrix -> T = [t1, t2, t3]
		-> determine the directions of local coordinate system
	5. transform global coordinates to local coordinate system
		-> P' = (P-c)T
	*6. find column-wise minimum values -> gives px_min, py_min, pz_min ( in local coordinates )
	*7. find column-wise maximum values -> gives px_max, py_max, pz_max ( P' )
		=> store in matrix Q' [8x3] (slide 13)
	8. transform back to global coordinate system Q = (Q' T^T) + c
*/

#pragma once

#ifdef BOUNDINGBOX_EXPORTS
#define BOUNDINGBOX_API __declspec(dllexport)
#else
#define BOUNDINGBOX_API __declspec(dllimport)
#endif

#include <vector>

namespace cpp
{

	class BoundingBoxComputer
	{
	public:
		BoundingBoxComputer();
		~BoundingBoxComputer();

		typedef double* doubleptr;

		static doubleptr BOUNDINGBOX_API computeAxisAlignedBoundingBox(double* input, int numberOfPoints);

		//Task 1: add a function to this class, called "computeOrientedBoundingBox"
		//Input parameters: "double* input, int numberOfPoints"
		//return type: doubleptr

		//YOUR CODE COMES HERE

		static doubleptr BOUNDINGBOX_API computeOrientedBoundingBox(double* input, int numberOfPoints);

		//Task 1 ends here
	};

}