#include "stdafx.h"
#include "BoundingBoxComputer.h"
#include "Eigen/Dense"
#include <algorithm>

#include <iostream>

namespace cpp
{ 

BoundingBoxComputer::BoundingBoxComputer()
{
}

BoundingBoxComputer::~BoundingBoxComputer()
{
}

double* BoundingBoxComputer::computeAxisAlignedBoundingBox(double* input, int numberOfPoints)
{
	typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> DoubleMatrix;
	DoubleMatrix matrixOfCoordinates = Eigen::Map<DoubleMatrix>(input, numberOfPoints, 3);
	
	auto maxCoordinates = matrixOfCoordinates.colwise().maxCoeff();
	auto minCoordinates = matrixOfCoordinates.colwise().minCoeff();

	double minX = minCoordinates(0);
	double minY = minCoordinates(1);
	double minZ = minCoordinates(2);

	double maxX = maxCoordinates(0);
	double maxY = maxCoordinates(1);
	double maxZ = maxCoordinates(2);

	DoubleMatrix resultMatrix(8, 3);

	double* result = new double[24] {
		    minX,minY,minZ,
			maxX,minY,minZ,
			maxX,maxY,minZ,
			minX,maxY,minZ,
			minX,minY,maxZ,
			maxX,minY,maxZ,
			maxX,maxY,maxZ,
			minX,maxY,maxZ
			};

	return result;
}

// This is the implementation of the function that you have added in BoundingBoxComputer.h
// You need to uncomment it and fill in the missing parts to get it work


double* BoundingBoxComputer::computeOrientedBoundingBox(double* input, int numberOfPoints)
{
	typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> DoubleMatrix;
	DoubleMatrix matrixOfCoordinates = Eigen::Map<DoubleMatrix>(input, numberOfPoints, 3);

	//matrixOfCoordinates contains the coordinates of the points in the following manner
	//
	// p_0x p_0y p0z
	// p_1x p_1y p_1z
	// ...
	// p_nx p_ny p_nz
	//
	//Where n is the total number of points

	//Task 2: Compute mean from the matrix of coordinates
	//Hint: use the colwise() and mean() member functions of matrixOfCoordinates

	auto mean = matrixOfCoordinates.colwise().mean();
	auto cx = mean(0);
	auto cy = mean(1);
	auto cz = mean(2);

	//Task 2 ends here

	//Task 3: Set up entries of the covariance matrix
	Eigen::Matrix3d covarianceMatrix = Eigen::Matrix3d::Zero();

	//Loop over the rows in matrixOfCoordinates
	for (size_t n = 0; n < numberOfPoints; n++)
	{
		//Compute dx = p_ix - mean_x    //changed i to n to match with slides
		//	      dy = p_iy - mean_y
		//        dz = p_iz - mean_z
		//Then, add the entries into the covariance matrix
		//e.g. covarianceMatrix(0,0) = covarianceMatrix(0,0) + dx * dx
		auto dx = matrixOfCoordinates(n, 0) - cx;
		auto dy = matrixOfCoordinates(n, 1) - cy;
		auto dz = matrixOfCoordinates(n, 2) - cz;

		covarianceMatrix(0, 0) += dx * dx;
		covarianceMatrix(0, 1) += dx * dy;
		covarianceMatrix(0, 2) += dx * dz;
		covarianceMatrix(1, 1) += dy * dy;
		covarianceMatrix(1, 2) += dy * dz;
		covarianceMatrix(2, 2) += dz * dz;
	}
	covarianceMatrix(1, 0) = covarianceMatrix(0, 1); //  since covariance
	covarianceMatrix(2, 0) = covarianceMatrix(0, 2); //  matrix is
	covarianceMatrix(2, 1) = covarianceMatrix(1, 2); //  symmetric
	covarianceMatrix = covarianceMatrix / ((double)numberOfPoints);
	//Task 3 ends here
	
	//Compute the eigenvectors
	Eigen::EigenSolver<Eigen::Matrix3d> eigensolver(covarianceMatrix);
	auto eigenValues = eigensolver.eigenvalues();
	auto complexEigenVectors = eigensolver.eigenvectors();
	Eigen::Matrix3d eigenMatrix = complexEigenVectors.real();
	//Computing the eigenvectors ends here

	//Task 4: transform the points to the coordinate system determined by the eigenmatrix
	auto transformedPoints = matrixOfCoordinates;
	//First, translate the points to the location of mean
	for (size_t n = 0; n < numberOfPoints; n++)
	{
		transformedPoints(n, 0) -= cx;
		transformedPoints(n, 1) -= cy;
		transformedPoints(n, 2) -= cz;

	}

	//Then, apply the rotation transformation determined by eigenMatrix
	transformedPoints = transformedPoints * eigenMatrix;
	//Task 4 ends here
	
	//Task 5: find the minimum and maximum values of x,y,z in the transformed setting
	//Hint: use the colwise(), maxCoeff(), and minCoeff(), just like it was used in computeAxisAlignedBoundingBox
	auto maxCoordinates = transformedPoints.colwise().maxCoeff();
	auto minCoordinates = transformedPoints.colwise().minCoeff();
	//Task 5 ends here

	//We can now store the resulting 8 points in an 8x3 matrix
	double minX = minCoordinates(0);
	double minY = minCoordinates(1);
	double minZ = minCoordinates(2);

	double maxX = maxCoordinates(0);
	double maxY = maxCoordinates(1);
	double maxZ = maxCoordinates(2);

	DoubleMatrix cornersOfBoundingBox(8,3);
	
	cornersOfBoundingBox <<
		minX, minY, minZ,
		maxX, minY, minZ,
		maxX, maxY, minZ,
		minX, maxY, minZ,
		minX, minY, maxZ,
		maxX, minY, maxZ,
		maxX, maxY, maxZ,
		minX, maxY, maxZ;

	//Task 6: The coordinates in cornersOfBoundingBox need to be transformed back to the original coordinate system
	//First, rotate using the transpose of eigenMatrix
	cornersOfBoundingBox = cornersOfBoundingBox * eigenMatrix.transpose();

	//Then apply the opposite of the translation in Task 4
	for (int n = 0; n< 8; n++) // <8 instead of < numberOfPoints
	{
		//cornersOfBoundingBox.row(i) = ...
		cornersOfBoundingBox(n, 0) += cx;
		cornersOfBoundingBox(n, 1) += cy;
		cornersOfBoundingBox(n, 2) += cz;
	}
	//Task 6 ends here

	double* result = new double[24];
	for (int i = 0; i < 24; i++)
	{
		result[i] = cornersOfBoundingBox.data()[i];
	}
	
	return result;
}

}
