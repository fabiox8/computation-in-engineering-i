#just some comment, so that ''' is not the first line

'''
-ask the user to select a surface
-compute bounding box (oriented/ axis aligned)
-visualize the result
'''


import sys
#IMPORTANT! If you work in the CIP pool, you need to copy the folder
#"P:\LS_ComputationInEngineering\SS2018_ComputerAidedDesign\IronPython" to a local folder on your computer (e.g. to the Desktop)
#Once it is copied, change the path in the following line so that it points to the "Lib" folder of IronPython on your local machine
#sys.path = [r"C:\Users\ga53hoc\Desktop\IronPython\Lib"] + sys.path

import clr
import rhinoscriptsyntax as rs
from Rhino.Geometry import *
from Rhino.Commands import Result
from Rhino.Input import RhinoGet
from Rhino.DocObjects import ObjectType
from Rhino.DocObjects import ObjRef

from System import Array

#In the following four lines, we import our interface dll
#Change this to the path where you built "BoundingBoxInterface.dll"
#sys.path.append(r"C:\Users\ga53hoc\Desktop\Compulsory Assignment\BoundingBox\x64\Release")
sys.path.append(r"E:\Dropbox\Kurse\2_Semester_SS_27Credits\CiE_2_6Credits\1. Official\Compulsory Assignment\BoundingBox\x64\Release")
clr.AddReference("BoundingBoxInterface.dll")
import BoundingBoxInterface

#Select a surface and mesh it
res, ref = RhinoGet.GetOneObject("Select surface or polysurface to mesh", True, ObjectType.Surface | ObjectType.PolysrfFilter)
brep = ref.Brep()
meshes = Mesh.CreateFromBrep(brep,MeshingParameters.Smooth)

#Convert the coordinates of the resulting mesh vertices
#into a type that our interface understands
nativeCoordinates = []
for mesh in meshes:
    for vertex in mesh.Vertices:
        nativeCoordinates.append(vertex.X)
        nativeCoordinates.append(vertex.Y)
        nativeCoordinates.append(vertex.Z)
nativeCoordinates = Array[float](nativeCoordinates)

boundingBoxComputer = BoundingBoxInterface.BoundingBoxComputer()

#Task 9: ask the user whether they want to compute an axis aligned or an oriented bounding box:
#if the user enters the integer "0" -> compute axis aligned
#                               "1" -> compute oriented
#Hint: find a method in rhinoscriptsyntax for getting integer user input
#http://developer.rhino3d.com/api/RhinoScriptSyntax/#userinterface
#methodChoice = ...

methodChoice = rs.GetInteger('Axis Aligned (0) or Oriented (1) BoundingBox?', None, 0, 1)

#Invoke the computation of the bounding box
#the result should be stored in the variable "result"
#if ...
#   compute axis aligned
#else
#   compute oriented

if methodChoice == 0:
    result = boundingBoxComputer.computeAxisAlignedBoundingBox(nativeCoordinates)
    print("computing AxisAlignedBoundingBox")
elif methodChoice == 1:
    result = boundingBoxComputer.computeOrientedBoundingBox(nativeCoordinates)
    print("computing OrientedBoundingBox")
else:
	print("error, wrong methodChoice")
	
#Task 9 ends here

#When Task 9 is completed, you can remove the following line of code
#Until then, we can only compute axis aligned bounding boxes

#result = boundingBoxComputer.computeAxisAlignedBoundingBox(nativeCoordinates)

#The resulting array of coordinates need to be converted back to
#a type that Rhino understands
points = []
for i in range(0,len(result))[::3]:
    currentPoint = [result[i],result[i+1],result[i+2]]
    points.append(currentPoint)

rs.AddBox(points)
