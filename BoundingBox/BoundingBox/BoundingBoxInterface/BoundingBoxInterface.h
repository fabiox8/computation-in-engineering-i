/*
interface library that can be imported in RhinoPython
	wraps functions of "BoundingBoxComputer" (link between RhinoPython & C++)
	"adapter code" which will interface BoundingBoxComputer to RhinoPython

compiled to .dll file

ThinoPython interpreter is written in C#
	make BoundingBoxComputer.dll understandable for Microsoft CLR
	
differences between pure C++ and CLR supporting C++:
	pointers in CLR behave like smart-pointers in C++
	marked by ^ instead of *
	allocated by "gcnew" instead of "new"
	garbage collector exists
*/

#pragma once

namespace BoundingBoxInterface
{
	public ref class BoundingBoxComputer
	{
	public:
		BoundingBoxComputer(); 
		~BoundingBoxComputer();

		array<double>^ computeAxisAlignedBoundingBox(array<double>^ vertices);

		//Task 7: add a function "computeOrientedBoundingBox" which will wrap the function
		//BoundingBoxComputer::computeOrientedBoundingBox
		//the parameters and the return type is the same as for computeAxisAlignedBoundingBox
		
		//YOUR CODE COMES HERE
		array<double>^ computeOrientedBoundingBox(array<double>^ vertices);
		//Task 7 ends here

	};
}