#include "BoundingBoxInterface.h"
#include "BoundingBox\BoundingBoxComputer.h"

#include <msclr/marshal_cppstd.h>

namespace BoundingBoxInterface
{
	
	BoundingBoxComputer::BoundingBoxComputer()
	{
	}

	BoundingBoxComputer::~BoundingBoxComputer()
	{
	}

	array<double>^ BoundingBoxComputer::computeAxisAlignedBoundingBox(array<double>^ vertices)
	{
		//Get a pointer to the first element in the vertices array
		pin_ptr<double> addressOfArray = &vertices[0];
		//Compute the number of points that were passed to this function
		int length = vertices->Length;
		int numberOfPoints = length / 3;

		//Invoke the computation of the axis aligned bounding box 
		double* rawResult = cpp::BoundingBoxComputer::computeAxisAlignedBoundingBox(addressOfArray, numberOfPoints);

		//Allocate memory for storing the result
		array<double>^ result = gcnew array<double>(24);
		//Copy the result returned by computeAxisAlignedBoundingBox into the result array
		System::Runtime::InteropServices::Marshal::Copy(System::IntPtr((void*)rawResult), result, 0, 24);

		delete rawResult;
		return result;
	}

	// This is the implementation of the function that you have added in BoundingBoxInterface.h
	// You need to uncomment it and fill in the missing parts to get it work

	array<double>^ BoundingBoxComputer::computeOrientedBoundingBox(array<double>^ vertices)
	{
		//Task 8: complete the missing parts in this function
		//Hint: check how the wrapper function "computeAxisAlignedBoundingBox" above is implemented

		//Get a pointer to the first element in the vertices array
		pin_ptr<double> addressOfArray = &vertices[0];
		//Compute the number of points that were passed to this function
		int length = vertices->Length;
		int numberOfPoints = length / 3;
		
		//Invoke "BoundingBoxComputer::computeOrientedBoundingBox" to compute the oriented bounding box
		double* rawResult = cpp::BoundingBoxComputer::computeOrientedBoundingBox(addressOfArray, numberOfPoints);


		//Task 8 ends here

		//Allocate memory for storing the result
		array<double>^ result = gcnew array<double>(24);
		//Copy the result returned by computeOrientedBoundingBox into the result array
		System::Runtime::InteropServices::Marshal::Copy(System::IntPtr((void*)rawResult), result, 0, 24);

		delete rawResult;

		return result;
	}
}