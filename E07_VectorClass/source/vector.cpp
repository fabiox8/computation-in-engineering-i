#include "vector.hpp"

#include <cmath>
#include <iostream>

vector::vector( int n, double* vec )
{
	size = n;
	entries = new double[size];
	for ( int i = 0; i < size; i++ )
	{
		entries[i] = vec[i];
	}	
}

vector::vector( const vector& vec )
{
	size = vec.size;
	entries = new double[vec.size];
	for ( int i = 0; i < vec.size; i++ )
	{
		entries[i] = vec.entries[i];
	}
}

vector& vector::operator=( const vector& vec )
{
	if ( this != &vec ) // prevent self assignment
	{
		for ( int i = 0; i < vec.size; i++)
		{
			entries[i] = vec.entries[i];
		}
		size = vec.size;
	}

	return *this; // * is the "dereference operator"
}

vector::~vector()
{
	delete[] entries;
	std::cout << "Destructor was called" << std::endl;
}

void vector::draw()
{
	for (int i = 0; i < size; i++)
	{
		std::cout << entries[i] << std::endl;
	}
}

double vector::one_norm()
{
	double one_norm = 0;
	for ( int i = 0; i < size; i++ )
	{
		one_norm += std::fabs( entries[i] );
	}
	return one_norm;
}

double vector::two_norm() {
	double two_norm = 0;
	for ( int i = 0; i < size; i++ )
	{
		two_norm += entries[i] * entries[i];
	}
	return std::sqrt( two_norm );
}

double vector::max_norm()
{
	double max_norm = entries[0];
	for ( int i = 1; i < size; i++ )
	{
		if ( entries[i] > max_norm ) { max_norm = entries[i]; }
	}
	return max_norm;
}