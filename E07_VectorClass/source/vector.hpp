class vector
{
private:
	double* entries;
	int size;
	
public:
	vector( int n, double* vec); //constructor
	vector( const vector& vec ); //copy constructor
	vector& operator=(const vector& vec); //assignment operator
	~vector(); //destructor
	
	void draw(); //member function, that writes vector to console
	double one_norm();
	double two_norm();
	double max_norm();
};