/*
	Define a vector class that contains a pointer for the entries, an integer for the 
	size of the vector, and one-, two-, and max-norm functions.
*/

#include "vector.hpp"

#include <iostream>

int main()
{
	int length;
	std::cout << "Vector length:" << std::endl;
	std::cin >> length;
	double* array = new double[length];
	for ( int i = 0; i < length; i++)
	{
		std::cout << "Entry number " << (i+1) << ":" << std::endl;
		std::cin >> array[i];
	}
	vector vec1(length, array); //call the constructor
	delete[] array; //free the dynamic memory of the entries array
	
	vec1.draw();
	
	std::cout << "One-norm of vec1: " << vec1.one_norm() << std::endl;
	std::cout << "Two-norm of vec1: " << vec1.two_norm() << std::endl;
	std::cout << "Max-norm of vec1: " << vec1.max_norm() << std::endl;
	
	vector vec2(vec1);
	vec2.draw();
	
	vector vec3 = vec1;
	vec3.draw();
}